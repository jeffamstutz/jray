// ======================================================================== //
// Copyright 2016 Jefferson Amstutz                                         //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "vec.h"

#include <stdexcept>
#include <fstream>

/*! helper function for debugging. write out given pixels in PPM format */
inline void writePPM(const std::string &fileName,
                     const jray::vec2i &size,
                     const uint32_t *pixels)
{
  FILE *file = fopen(fileName.c_str(), "w");

  if (!file)
    throw std::runtime_error("writePPM() : could not open file " + fileName);

  fprintf(file, "P6\n%i %i\n255\n", size.x, size.y);

  for (int i = 0; i < size.x*size.y; i++) {
    char *ptr = (char*)&pixels[i];
    fwrite(ptr, 1, 3, file);
  }

  fclose(file);
}
