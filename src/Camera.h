// ======================================================================== //
// Copyright 2016 Jefferson Amstutz                                         //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "Ray.h"

namespace jray {

template <typename T>
class Camera_T
{
public:
  using vec3  = vec_t<T, 3>;
  using ray_t = Ray_T<T>;

  ray_t getRayAt(int i);
  ray_t getRayAt(int x, int y);
  ray_t getRayAt(vec2i dims);

  void setCamera(vec3 eye, vec3 dir, vec3 up, bool orthographic = false);
  void setDimensions(int width, int height);

  int   totalRays();
  vec2i dims();

private:

  // Helper type //

  struct ViewData
  {
    vec3 eye;
    vec3 u;
    vec3 v;
    vec3 w;
    bool ortho{false};
  } m_view;

  // Private data //

  T m_ixres {0};
  T m_iyres {0};

  T m_dx {0};
  T m_dy {0};

  T m_xinit {0};
  T m_yinit {0};

  vec2i m_dims;
};

using Camera = Camera_T<float>;

// Inline function definitions ////////////////////////////////////////////////

template <typename T>
inline typename Camera_T<T>::ray_t Camera_T<T>::getRayAt(int i)
{
  const int y = i / m_dims.y;
  const int x = i % m_dims.y;

  return getRayAt(x, y);
}

template <typename T>
inline typename Camera_T<T>::ray_t Camera_T<T>::getRayAt(int x, int y)
{
  const float xf = m_xinit + x*m_dx;
  const float yf = m_yinit + y*m_dy;

  ray_t ray;

  if (m_view.ortho)
  {
    ray.org = m_view.eye + xf*m_view.u + yf*m_view.w + m_view.v;
    ray.dir = m_view.v;
    normalize(ray.dir);
  }
  else
  {
    ray.org = m_view.eye;
    ray.dir = xf*m_view.u + yf*m_view.w + m_view.v;
    normalize(ray.dir);
  }

  return ray;
}

template <typename T>
inline typename Camera_T<T>::ray_t Camera_T<T>::getRayAt(vec2i dims)
{
  return getRayAt(dims.x, dims.y);
}

template <typename T>
inline void Camera_T<T>::setCamera(vec3 eye, vec3 dir, vec3 up, bool ortho)
{
  vec3 v = jray::normalize(dir - eye);
  vec3 u = jray::normalize(jray::cross(v, up));
  vec3 w = jray::normalize(jray::cross(u, v));

  T fov  = 60.f;
  T ylen = tan(0.5f*fov*M_PI/180.f);
  T xlen = (ylen*IMAGE_DIMS)/IMAGE_DIMS;

  u *= xlen;
  w *= ylen;

  m_view.eye   = eye;
  m_view.u     = u;
  m_view.v     = v;
  m_view.w     = w;
  m_view.ortho = ortho;
}

template <typename T>
inline void Camera_T<T>::setDimensions(int width, int height)
{
  m_dims.x = width;
  m_dims.y = height;

  m_ixres = 1.f/width;
  m_iyres = 1.f/height;

  m_dx = 2.f*m_ixres;
  m_dy = 2.f*m_iyres;

  m_xinit = -1.f + m_ixres;
  m_yinit = -1.f + m_iyres;
}

template <typename T>
inline int Camera_T<T>::totalRays()
{
  return m_dims.x * m_dims.y;
}

}// namespace jray
