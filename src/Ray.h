// ======================================================================== //
// Copyright 2016 Jefferson Amstutz                                         //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "vec.h"

#include <limits>

namespace jray {

template<typename T>
struct Ray_T {
  using vec3 = vec_t<T, 3>;

  vec3 org;
  vec3 dir;

  T tnear {0};
  T tfar {std::numeric_limits<float>::infinity()};

  vec3 pointAt(T t_value);
};

using Ray = Ray_T<float>;

// Inlined template definitions //

template<typename T>
inline typename Ray_T<T>::vec3 Ray_T<T>::pointAt(T t_value)
{
  return org + t_value * dir;
}

}// namespace jray
