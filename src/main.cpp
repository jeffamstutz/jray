// ======================================================================== //
// Copyright 2016 Jefferson Amstutz                                         //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#define IMAGE_DIMS 1024

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include "Camera.h"
#include "Sphere.h"
#include "util.h"

int main()
{
  // Create and fill framebuffer
  vector<uint32_t> fb(IMAGE_DIMS*IMAGE_DIMS);

  // Create and initialize camera
  jray::Camera camera;
  camera.setDimensions(IMAGE_DIMS, IMAGE_DIMS);

  jray::vec3f eye{0.f, 0.f, 0.f};
  jray::vec3f dir{0.f, 0.f, 1.f};
  jray::vec3f up{0.f, 1.f, 0.f};

  camera.setCamera(eye, dir, up);

  // Create spheres
  vector<jray::Sphere> spheres;
  spheres.push_back({jray::vec3f(0.f, 0.f, 3.f), 1.f});

  // Trace rays
  for (size_t i = 0; i < fb.size(); ++i) {
    auto ray = camera.getRayAt(i);
    fb[i] = 0x11111111;// bg color
    for ( auto &sphere : spheres) {
#if 0
      std::cout << "Intersecting sphere: " << sphere.center << " : "
                << sphere.radius << std::endl;
#endif
      if (sphere.hit(ray)) {
        fb[i] = 0xFFFFFFFF;
        break;
      }
    }
  }

  // Write output image
  writePPM("output.ppm", {IMAGE_DIMS, IMAGE_DIMS}, fb.data());

  return 0;
}
