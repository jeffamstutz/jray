// ======================================================================== //
// Copyright 2016 Jefferson Amstutz                                         //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "vec.h"
#include "Ray.h"

namespace jray {

template<typename T>
struct Sphere_T {
  using vec3  = vec_t<T, 3>;
  using ray_t = Ray_T<T>;

  vec3 center;
  T    radius;

  bool hit(ray_t &ray);
};

using Sphere = Sphere_T<float>;

// Inlined template definitions //

template<typename T>
inline bool Sphere_T<T>::hit(Sphere_T<T>::ray_t &ray)
{
  const vec3 A = center - ray.org;

  const T a = dot(ray.dir, ray.dir);
  const T b = 2.f*dot(ray.dir, A);
  const T c = dot(A,A)-radius*radius;

  const T radical = b*b - 4.f*a*c;
  if (radical < T(0)) return false;

  const T srad = sqrt(radical);

  const T t_in  = (b - srad) * (1.f/(2.f*a));
  const T t_out = (b + srad) * (1.f/(2.f*a));

  if (t_in > ray.tnear && t_in < ray.tfar) {
    return true;
    //ray.t = t_in;
  } else if (t_out > ray.tnear && t_out < ray.tfar) {
    return true;
    //ray.t = t_out;
  }
#if 0
  if (hit) {
    ray.primID = primID;
    ray.geomID = geometry->geometry.geomID;
    ray.instID = -1;
    // cannot easily be moved to postIntersect
    // we need hit in object space, in postIntersect it is in world-space
    ray.Ng = ray.org + ray.t*ray.dir - center;
  }
#endif

  return false;
}

}// namespace jray
